/*
 * IR.c
 *
 * Created: 07/01/2018 00:04:36
 *  Author: krzys
 */ 
#include "IR.h"

uint8_t receive_data;
uint8_t last_port_value;
uint8_t receive_bit_address;
uint8_t active_pin;
uint8_t transition_type;
uint8_t hit_points;
unsigned long long last_receive_ticks;
unsigned long long suspended;

void IR_on(void){
	DDRA |= _BV(DDA7);
}

void IR_off(void){
	DDRA &= ~_BV(DDA7);
}

void IR_init(){
	receive_data = 0;
	last_port_value = 0;
	receive_bit_address = 0;
	active_pin = 255;
	transition_type = 0;
	hit_points = 20;
	last_receive_ticks = get_tim0_ticks();
}

void IR_send_bit(uint8_t bit){
	unsigned long long ticks = get_tim0_ticks();
	if(bit){
		IR_on();
		while(get_tim0_ticks() < ticks + BIT_1_DURATION);
		IR_off();
		while(get_tim0_ticks() < ticks + BIT_FULL_PEIOD);
	}
	else{
		IR_on();
		while(get_tim0_ticks() < ticks + BIT_0_DURATION);
		IR_off();
		while(get_tim0_ticks() < ticks + BIT_FULL_PEIOD);
	}
	
}

void IR_fire(void){
	GIMSK &= ~_BV(PCIE0);
	IR_send_bit(0);
	IR_send_bit(0);
	IR_send_bit(1);
	IR_send_bit(0);
	IR_send_bit(1);
	IR_send_bit(1);
	IR_send_bit(1);
	IR_send_bit(0);
	GIMSK |= _BV(PCIE0);
}

uint8_t IR_receive(uint8_t port){
	unsigned long long ticks = get_tim0_ticks();
	if(--hit_points<=1)death(ticks);
	if(hit_points<=10){
		suspended = ticks;
	}
	
	return 0;
} 

void death(unsigned long long t){
	GIMSK &= ~_BV(PCIE0);
	while(t + RESPAWN_TIME > get_tim0_ticks()); //do nothing while being dead.
	hit_points = 20;
	GIMSK |= _BV(PCIE0);
}