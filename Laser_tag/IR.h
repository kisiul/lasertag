/*
 * IR.h
 *
 * Created: 07/01/2018 00:04:53
 *  Author: krzys
 */ 


#ifndef IR_H_
#define IR_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "clocks.h"



#define TEAM alpha
#define REG 1

#define RESPAWN_TIME 1000

#define RECEIVE_TIMEOUT 150
#define BIT_0_DURATION 10
#define BIT_1_DURATION 90
#define BIT_FULL_PEIOD 100
#define DATA_VALIDATION_BIT 5

void IR_on(void);
void IR_off(void);
void IR_init(void);
void IR_fire(void);
uint8_t IR_receive(uint8_t port);
void death(unsigned long long t);

#endif /* IR_H_ */