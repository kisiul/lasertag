/*
 * clocks.c
 *
 * Created: 07/01/2018 00:05:33
 *  Author: krzys
 */ 

#include "clocks.h"

volatile unsigned long long tim0_ticks; 

ISR(TIM0_COMPA_vect){
	tim0_ticks++;
}

void clocks_init(void){
	   TCCR0A |= _BV(COM0B0);
	   TCCR0A |= _BV(WGM01);
	   
	   TCCR0B |= _BV(CS00);
	   
	   TIMSK0 |= _BV(TOIE0);
	   TIMSK0 |= _BV(OCIE0A);
	   OCR0A = 98;
	   
	   tim0_ticks = 0;
}

void reset_tim0_ticks(void){
	tim0_ticks = 0;
}

unsigned long long get_tim0_ticks(void){
	return tim0_ticks;
}