/*
 * Laser_tag.c
 *
 * Created: 05/01/2018 23:38:12
 * Author : Krzystzof Dabrowski
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "IR.h"
#include "gpio.h"
#include "clocks.h"

int main(void)
{

   gpio_init();
   clocks_init();
   
   sei();
 
    while (1) 
    {

		IR_off();
		gpio_reset(PORT_LED, PIN_LED);
		if(gpio_read(PORT_TRIGG, PIN_TRIGG)){
			gpio_reset(PORT_LED, PIN_LED);
		}
		else{
			gpio_set(PORT_LED, PIN_LED);
			IR_fire();
			reset_tim0_ticks();
			
			gpio_reset(PORT_LED, PIN_LED);

			while(!gpio_read(PORT_TRIGG, PIN_TRIGG));
		}
    }
}

