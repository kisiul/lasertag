/*
 * clocks.h
 *
 * Created: 07/01/2018 00:05:17
 *  Author: krzys
 */ 


#ifndef CLOCKS_H_
#define CLOCKS_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void clocks_init(void);
void reset_tim0_ticks(void);
unsigned long long get_tim0_ticks(void);

#endif /* CLOCKS_H_ */