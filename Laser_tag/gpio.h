/*
 * gpio.h
 *
 * Created: 07/01/2018 00:05:59
 *  Author: krzys
 */ 


#ifndef GPIO_H_
#define GPIO_H_

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#define PORT_TRIGG 'A'
#define PIN_TRIGG 0

#define PORT_LED 'A'
#define PIN_LED 6

bool gpio_read(char port, int pin);
void gpio_init(void);
void gpio_set(char port, int pin);
void gpio_reset(char port, int pin);

#endif /* GPIO_H_ */