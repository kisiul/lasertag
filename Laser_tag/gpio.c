/*
 * gpio.c
 *
 * Created: 07/01/2018 00:05:49
 *  Author: krzys
 */ 
#include "gpio.h"
#include "IR.h"
ISR(PCINT0_vect){
	gpio_set(PORT_LED, PIN_LED);
	IR_receive(~((PINA>>2)&0x0f));
}

bool gpio_read(char port, int pin){
	switch(port){
		case 'A':
			if((PINA&_BV(pin))==1)return true;
			else return false;
		case 'B':
			if((PINB&_BV(pin))==1)return true;
			else return false;
		default:
			return false;
	}
}

void gpio_init(void){
	   DDRA |= _BV(DDA6);
	   DDRA |= _BV(DDA7);
	   PORTA = 0;
	   
	   DDRB |= _BV(DDB0);
	   DDRB |= _BV(DDB1);
	   
	   PORTB = 0;
	   
	   /*interrupts:*/
	   GIMSK |= _BV(PCIE0);
	   PCMSK0 |= _BV(PCINT2);
	   PCMSK0 |= _BV(PCINT3);
	   PCMSK0 |= _BV(PCINT4);
	   PCMSK0 |= _BV(PCINT5);
}

void gpio_set(char port, int pin){
	switch(port){
		case 'A':
			PORTA = _BV(pin);
		break;
		case 'B':
			PORTB = _BV(pin);
		break;
	}
}

void gpio_reset(char port, int pin){
		switch(port){
			case 'A':
				PORTA &= ~_BV(pin);
			break;
			case 'B':
				PORTB &= ~_BV(pin);
			break;
		}
}